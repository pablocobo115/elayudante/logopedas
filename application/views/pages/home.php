<div class="Primer-Bloque col-lg-12">
    <div class="row">
        <div id="carouselExampleFade" class="carousel carousel-fade col-sm-7" data-bs-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="<?php echo base_url(); ?>assets/images/png/prueba1.jpeg" class="d-block w-100 rounded mx-auto d-block">
                </div>
                <div class="carousel-item">
                    <img src="<?php echo base_url(); ?>assets/images/png/prueba2.png" class="d-block w-100 rounded mx-auto d-block">
                </div>
                <div class="carousel-item">
                    <img src="<?php echo base_url(); ?>assets/images/png/prueba3.png" class="d-block w-100 rounded mx-auto d-block">
                </div>
            </div>
            <a class="carousel-control-prev" role="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="false"></span>
                <span class="visually-hidden"></span>
            </a>
            <a class="carousel-control-next" role="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="false"></span>
                <span class="visually-hidden"></span>
            </a>
        </div>
        <div class="col-md-5 container">
            <div class="row row-cols-1 row-cols-md-2">
                <div class="col mb-4">
                    <a class="dropdown-item" href="<?php echo base_url(); ?>encuentra">
                        <div class="card">
                            <img src="<?php echo base_url(); ?>assets/images/png/encuentra.gif" class="card-img-top" alt="..." height="200">
                        </div>
                    </a>
                </div>
                <div class="col mb-4">
                    <a class="dropdown-item" href="<?php echo base_url(); ?>faq">
                        <div class="card">
                            <img src="<?php echo base_url(); ?>assets/images/png/faq.png" class="card-img-top" alt="..." height="200">
                        </div>
                    </a>
                </div>
                <div class="col mb-4">
                    <a class="dropdown-item" href="<?php echo base_url(); ?>acuerdos">
                        <div class="card">
                            <img src="<?php echo base_url(); ?>assets/images/png/acuerdos.gif" class="card-img-top" alt="..." height="200">
                        </div>
                    </a>
                </div>
                <div class="col mb-4">
                    <a class="dropdown-item" href="<?php echo base_url(); ?>cplc">
                        <div class="card">
                            <img src="<?php echo base_url(); ?>assets/images/png/formacion.gif" class="card-img-top" alt="..." height="200">
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
 
<div class="col-lg-12 py-5" id="segundoBloque"  style="background-image: url('https://cdn.pixabay.com/photo/2016/01/16/01/00/blue-1142745_960_720.jpg'); background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="card bg-transparent border-white text-white" style="backdrop-filter: blur(8px);">
                    <div class="card-body">
                        <h4 class="card-title">Servicios al logopeda</h4>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card bg-transparent border-white text-white" style="backdrop-filter: blur(8px);">
                    <div class="card-body">
                        <h4 class="card-title">Servicios al ciudadano</h4>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">