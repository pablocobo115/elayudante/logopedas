<html>
    <head>
        <title>Logopedas</title>
        <link rel="stylesheet" href="https://bootswatch.com/4/united/bootstrap.min.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/v4-shims.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
        <link rel="icon" href="<?=base_url()?>assets/images/png/favicon.ico" type="image/vnd.microsfot.icon">
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css"/>

    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="<?php echo base_url(); ?>"><img class="logo_header" src="/logopedas/assets/images/png/logo_header.png"></img></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            
            <div class="collapse navbar-collapse" id="navbarColor03">
                <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url(); ?>">INICIO 
                    <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">EL COLEGIO</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="<?php echo base_url(); ?>colegio">COLEGIO</a>
                            <a class="dropdown-item" href="<?php echo base_url(); ?>juntagob">JUNTA DE GOBIERNO</a>
                            <a class="dropdown-item" href="<?php echo base_url(); ?>estatutos">ESTATUTOS</a>
                            <a class="dropdown-item" href="<?php echo base_url(); ?>codigo">CÓDIGO DEONTOLÓGICO</a>
                            <a class="dropdown-item" href="<?php echo base_url(); ?>normativa">NORMATIVA</a>
                        </div>
                </li>
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">LOGOPEDAS</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="<?php echo base_url(); ?>logopedas">LOGOPEDAS</a>
                            <a class="dropdown-item" href="<?php echo base_url(); ?>serviciocolegiado">SERVICIO AL COLEGIADO</a>
                            <a class="dropdown-item" href="<?php echo base_url(); ?>acuerdos">ACUERDOS CON EMPRESAS</a>
                            <a class="dropdown-item" href="<?php echo base_url(); ?>foro">FORO</a>
                            <a class="dropdown-item" href="<?php echo base_url(); ?>videonoticias">VIDEONOTICIAS</a>
                        </div>
                </li>
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">FORMACIÓN</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="<?php echo base_url(); ?>formacion">FORMACIÓN</a>
                            <a class="dropdown-item" href="<?php echo base_url(); ?>cplc">CURSOS CPLC</a>
                            <a class="dropdown-item" href="<?php echo base_url(); ?>otroscursos">OTROS CURSOS</a>
                            <a class="dropdown-item" href="<?php echo base_url(); ?>masteryp">MÁSTER Y POSTGRADO</a>
                        </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url(); ?>faq">PREGUNTAS FRECUENTES</a>
                </li>
                </ul>
            </div>
<<<<<<< HEAD
=======
<<<<<<< HEAD
            <?php if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true) : ?>
                <a href="<?= base_url('users/logout_success') ?>" class="btn btn-info my-2 my-sm-0" type="submit">SALIR</a>
            <?php else : ?>
                <a href="<?php echo base_url(); ?>users/register" class="btn btn-info my-2 my-sm-0" type="submit" style="margin-right: 5;">COLÉGIATE</a>
                <a href="<?php echo base_url(); ?>users/login" class="btn btn-info my-2 my-sm-0" type="submit"  style="margin-right: 5;">ACCESO A COLEGIADOS</a>
            <?php endif; ?>
        </nav>
        <div class="container">
           
=======
>>>>>>> 8b55985896cd3d351894ecbc3e795a2b9fb684af
<<<<<<< HEAD
            <a href="<?php echo base_url(); ?>users/register" class="btn btn-info my-2 my-sm-0" type="submit" style="margin-right: 5;">COLÉGIATE</a>
            <a href="<?php echo base_url(); ?>users/login" class="btn btn-info my-2 my-sm-0" type="submit" style="margin-right: 5;">ACCESO A COLEGIADOS</a>
            <a href="<?php echo base_url(); ?>users/logout" class="btn btn-info my-2 my-sm-0" type="submit">SALIR</a>
            
=======
            <?php if(!$this->session->userdata('logged_in')) : ?>
                <a href="<?php echo base_url(); ?>users/register" class="btn btn-info my-2 my-sm-0" type="submit" style="margin-right: 5;">COLÉGIATE</a>
                <a href="<?php echo base_url(); ?>users/login" class="btn btn-info my-2 my-sm-0" type="submit"  style="margin-right: 5;">ACCESO A COLEGIADOS</a>
            <?php endif; ?>
            <?php if($this->session->userdata('logged_in')) : ?>
                <a href="<?php echo base_url(); ?>" class="btn btn-info my-2 my-sm-0" type="submit">SALIR</a>
                <a href="<?php echo base_url(); ?>serviciocolegiado" class="btn btn-info my-2 my-sm-0" type="submit">SERVICIOS AL COLEGIADO</a>
            <?php endif; ?>

>>>>>>> 9f6a9fca4de0cf133cd56934f10f67d227ef3b33
        </nav>
        <div class="container">
            <!-- Mensajes de registro o login -->
            <?php if($this->session->flashdata('user_registered')): ?>
                <?php echo '<p class="alert alert-primary">'.$this->session->flashdata('user_registered').'</p>'; ?>
            <?php endif; ?>

            <?php if($this->session->flashdata('login_failed')): ?>
                <?php echo '<p class="alert alert-danger">'.$this->session->flashdata('login_failed').'</p>'; ?>
            <?php endif; ?>

            <?php if($this->session->flashdata('user_loggedin')): ?>
                <?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_loggedin').'</p>'; ?>
            <?php endif; ?>

<<<<<<< HEAD
            <?php if($this->session->flashdata('user_loggedout')): ?>
                <?php echo '<p class="alert alert-primary">'.$this->session->flashdata('user_loggedout').'</p>'; ?>
=======
            <!-- Mensajes de registro o login -->
            <?php if($this->session->flashdata('login_failed')): ?>
                <?php echo '<p class="alert alert-danger">'.$this->session->flashdata('login_failed').'</p>'; ?>
            <?php endif; ?>

            <?php if($this->session->flashdata('user_loggedout')): ?>
                <?php echo '<p class="alert alert-danger">'.$this->session->flashdata('user_loggedout').'</p>'; ?>
>>>>>>> 9f6a9fca4de0cf133cd56934f10f67d227ef3b33
<<<<<<< HEAD
            <?php endif; ?>
=======
            <?php endif; ?>
>>>>>>> f058b846fd60ec0684f8cdad1eb3b6bfa6d0d88d
>>>>>>> 8b55985896cd3d351894ecbc3e795a2b9fb684af
