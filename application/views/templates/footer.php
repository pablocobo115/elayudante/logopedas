        </div>
        <footer class="section bg-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="">
                            <img class="img-fluid" id="imagenfooter" src="<?php echo base_url(); ?>assets/images/png/CPLC_IMAGOTIPO_VERT_BALNCO.png"></img>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="">
                            <h6 class="fw-bold footer-heading text-uppercase text-white">Ressources</h6>
                            <ul class="list-unstyled footer-link mt-4">
                                <li><a class="text-white" href="">Monitoring Grader </a></li>
                                <li><a class="text-white" href="">Video Tutorial</a></li>
                                <li><a class="text-white" href="">Term &amp; Service</a></li>
                                <li><a class="text-white" href="">Zeeko API</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="">
                            <h6 class="fw-bold footer-heading text-uppercase text-white">Ayuda</h6>
                            <ul class="list-unstyled footer-link mt-4">
                                <li><a class="text-white" href="">Date de alta</a></li>
                                <li><a class="text-white" href="">Accede</a></li>
                                <li><a class="text-white" href="<?php echo base_url(); ?>aviso">Aviso Legal</a></li>
                                <li><a class="text-white" href="<?php echo base_url(); ?>privacidad">Política de Privacidad</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="">
                            <h6 class="fw-bold footer-heading text-uppercase text-white">Contact Us</h6>
                            <p class="contact-info mt-4 text-white">Contact us if need help withanything</p>
                            <p class="contact-info text-white">+01 123-456-7890</p>
                            <div class="mt-5">
                                <ul class="list-inline">
                                    <li class="list-inline-item"><a href="https://es-es.facebook.com/colegiologopedascantabria/"><i class="fab facebook footer-social-icon fa-facebook-f"></i></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="fab linkedin footer-social-icon fa-linkedin-in"></i></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="fas mail footer-social-icon fa-envelope"></i></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="fas place footer-social-icon fa-location-arrow"></i></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="text-center mt-5">
                <p class="footer-alt mb-0 f-14" id="copyimagen">&copy; <?php echo date("Y"); ?> Desarrollado por <a class="text-light" href="https://www.elayudante.es/" target="_blank">ElAyudante</a></p>
            </div>
    </footer>
    </body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    <script src="//code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script>
       $(document).ready(function() {
            $('#mitabla').DataTable( {
                columnDefs: [ {
                    targets: [ 0 ],
                    orderData: [ 0, 1 ]
                }, {
                    targets: [ 1 ],
                    orderData: [ 1, 0 ]
                }, {
                    targets: [ 3 ],
                    orderData: [ 3, 0 ]
                } ],
                language: {
                    "decimal":        "",
                    "emptyTable":     "No hay datos",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(Filtro de _MAX_ total registros)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron coincidencias",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Ultimo",
                        "next":       "Próximo",
                        "previous":   "Anterior"
                    },
                    "aria": {
                        "sortAscending":  ": Activar orden de columna ascendente",
                        "sortDescending": ": Activar orden de columna desendente"
                    }
                }
            } );
        } );
    </script>
</html>