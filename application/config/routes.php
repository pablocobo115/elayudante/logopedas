<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'pages/view';

$route['users/login'] = 'users/login';
$route['users/register'] = 'users/register';
$route['users/logout'] = 'users/logout';

$route['(:any)'] = 'pages/view/$1';
$route['404_override'] = 'errors';
$route['translate_uri_dashes'] = FALSE;