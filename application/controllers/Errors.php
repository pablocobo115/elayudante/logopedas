<?php

// Error controller
// This controller is used to manage the errors (404)
class Errors extends CI_Controller 
{
        public function __construct() 
    {
        parent::__construct(); 
    }
    // Main controller for the contact form
    public function index()
    {
        // Create your custom controller

        // Display page
        $this->load->view('templates/header');
        $this->load->view('errors/error404');
        $this->load->view('templates/footer');
    }
}