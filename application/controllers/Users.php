<?php
<<<<<<< HEAD
=======
<<<<<<< HEAD
    defined('BASEPATH') OR exit('No direct script access allowed');
    class Users extends CI_Controller{

        public function __construct(){
=======
>>>>>>> 8b55985896cd3d351894ecbc3e795a2b9fb684af
<<<<<<< HEAD
class Users extends CI_Controller {

    public function register(){
        $data['title'] = 'Registrate';

        $this->form_validation->set_rules('nombre', 'Nombre', 'required');

        $this->form_validation->set_rules('usuario', 'Usuario', 'required|callback_check_username_exists');

        $this->form_validation->set_rules('email', 'Email', 'required|callback_check_email_exists');

        $this->form_validation->set_rules('pass', 'Password', 'required');

        $this->form_validation->set_rules('pass2', 'Confirma Password', 'matches[pass]');

        if($this->form_validation->run()===FALSE){
            $this->load->view('templates/header');
            $this->load->view('users/register', $data);
            $this->load->view('templates/footer');
        } else {
            $enc_password = md5($this->input->post('password'));

            $this->user_model->register($enc_password);

            $this->session->set_flashdata('user_registered', 'Ya estás registrado. Ahora puedes logarte');

            redirect('home');
        }
    }

    public function login(){
        $data['title'] = 'Acceso';

        $this->form_validation->set_rules('usuario', 'Usuario', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if($this->form_validation->run() === FALSE){
            $this->load->view('templates/header');
            $this->load->view('users/login', $data);
            $this->load->view('templates/footer');
        } else {
            
            // Get username
            $username = $this->input->post('usuario');
            // Get and encrypt the password
            $password = md5($this->input->post('password'));

            // Login user
            $user_id = $this->user_model->login($username, $password);

            if($user_id){
                // Create session
                $user_data = array(
                    'user_id' => $user_id,
                    'username' => $username,
                    'logged_in' => true
                );

                $this->session->set_userdata($user_data);

                // Set message
                $this->session->set_flashdata('user_loggedin', 'You are now logged in');

                redirect('posts');
            } else {
                // Set message
                $this->session->set_flashdata('login_failed', 'Login is invalid');

                redirect('users/login');
            }		
        }
    }

    public function logout(){
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('username');

        $this->session->set_flashdata('user_loggedout', 'Hasta pronto.');

        redirect('users/login');
    }

    public function check_username_exists($username){
        $this->form_validation->set_message('check_username_exists', 'Este usuario ya existe. Por favor, escoja otro.');
        if($this->user_model->check_username_exists($username)){
            return true;
        } else {
            return false;
        }
    }

    public function check_email_exists($email){
        $this->form_validation->set_message('check_email_exists', 'Este email ya existe. Por favor, escoja otro.');
        if($this->user_model->check_email_exists($email)){
            return true;
        } else {
            return false;
        }
    }
}
=======
	class Users extends CI_Controller{
<<<<<<< HEAD
=======
>>>>>>> f058b846fd60ec0684f8cdad1eb3b6bfa6d0d88d
>>>>>>> 8b55985896cd3d351894ecbc3e795a2b9fb684af

		public function __construct(){
			parent::__construct();
		}

		// Register user
		public function register(){
			$data['title'] = 'Sign Up';

			$this->form_validation->set_rules('name', 'Nombre', 'required');
			$this->form_validation->set_rules('username', 'Código Postal', 'required|callback_check_username_exists');
			$this->form_validation->set_rules('email', 'Email', 'required|callback_check_email_exists|valid_email');
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('password2', 'Confirmar Password', 'matches[pass]');

			if($this->form_validation->run() === FALSE){
				$this->load->view('templates/header');
				$this->load->view('users/register', $data);
				$this->load->view('templates/footer');
			} else {
				// Encrypt password
				$enc_password = md5($this->input->post('password'));

				$this->user_model->register($enc_password);

				// Set message
				$this->session->set_flashdata('user_registered', 'Bienvenido al Colegio de Logopedas. Ya puedes logarte');

				redirect('home');
			}
		}

		// Log in user
		public function login(){
			$data['title'] = 'Área de acceso';

			$this->form_validation->set_rules('username', 'Usuario', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');

			$this->load->helper(array('form', 'url'));

			if($this->form_validation->run() === FALSE){
				$this->load->view('templates/header');
				$this->load->view('users/login', $data);
				$this->load->view('templates/footer');
			} else {
				$this->load->model('user_model');
				// Get username
				$username = $this->input->post('username');
				// Get and encrypt the password
				$password = md5($this->input->post('password'));

				// Login user
				$user_id = $this->user_model->login($username, $password);

				if($user_id){
					// Create session
					$user_data = array(
						'user_id' => $user_id,
						'username' => $username,
						'logged_in' => true
					);

					$this->session->set_userdata($user_data);

					// Set message
					$this->session->set_flashdata('user_loggedin', 'Ahora puedes acceder a tu área personal');

					redirect('servicioscolegiado');
				} else {
					// Set message
					$this->session->set_flashdata('login_failed', 'Vuelve a intentar el acceso');

					redirect('users/login');
				}		
			}
		}

		// Log user out
<<<<<<< HEAD
=======
<<<<<<< HEAD
		public function logout() {
		
            // create the data object
            $data = new stdClass();
            
            if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {
                
                // remove session datas
                foreach ($_SESSION as $key => $value) {
                    unset($_SESSION[$key]);
                }
                
                // user logout ok
                $this->load->view('header');
                $this->load->view('users/logout/logout_success', $data);
                $this->load->view('footer');
                
            } else {
                
                // there user was not logged in, we cannot logged him out,
                // redirect him to site root
                redirect('/');
                
            }
            
        }
    }
=======
>>>>>>> 8b55985896cd3d351894ecbc3e795a2b9fb684af
		public function logout(){
			// Unset user data
			$this->session->unset_userdata('logged_in');
			$this->session->unset_userdata('user_id');
			$this->session->unset_userdata('username');

			// Set message
			$this->session->set_flashdata('user_loggedout', 'Hasta pronto');

			redirect('users/login');
		}

		// Check if username exists
		public function check_username_exists($username){
			$this->form_validation->set_message('check_username_exists', 'El usuario ya existe. Por favor, escoje otro.');
			if($this->user_model->check_username_exists($username)){
				return true;
			} else {
				return false;
			}
		}

		// Check if email exists
		public function check_email_exists($email){
			$this->form_validation->set_message('check_email_exists', 'El email ya existe. Por favor, escoje otro.');
			if($this->user_model->check_email_exists($email)){
				return true;
			} else {
				return false;
			}
		}
	}
>>>>>>> 9f6a9fca4de0cf133cd56934f10f67d227ef3b33
<<<<<<< HEAD
=======
>>>>>>> f058b846fd60ec0684f8cdad1eb3b6bfa6d0d88d
>>>>>>> 8b55985896cd3d351894ecbc3e795a2b9fb684af
